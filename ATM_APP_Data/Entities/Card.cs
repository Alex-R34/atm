﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATM_APP_Data.Entities
{
    public class Card
    {
        public Guid Id { get; set; }
        public string Number { get; set; }
        public string PinCode { get; set; }
        public decimal Balance { get; set; }
        public int FailedAttemps { get; set; }
        public bool IsBlocked { get; set; }

        public List<Operations> Operations { get; set; }

    }
}
