﻿using ATM_APP_Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATM_APP_Data.Entities
{
    public class Operations
    {
        public Guid Id { get; set; }
        public string CardNumber { get; set; }
        public DateTime DateTime { get; set; }
        public decimal? WithdrawalAmount { get; set; }
        public Operation Operation { get; set; }

        public Guid? CardId { get; set; }
        public Card Card { get; set; }
    }
}
