﻿using ATM_APP_Data.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATM_APP_Data.Context
{
    public class AtmContext : DbContext
    {
        public AtmContext(DbContextOptions<AtmContext> options) : base(options)
        {
            Database.EnsureCreated();
        }

        public DbSet<Card> Cards { get; set; }
        public DbSet<Operations> Operations { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Card>()
                .HasMany(x => x.Operations)
                .WithOne(x => x.Card)
                .HasForeignKey(x => x.CardId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<Card>().HasData(
                new Card { Id = Guid.NewGuid(),
                           Balance = 10000,
                           Number = "1111111111111111",
                           IsBlocked = false,
                           PinCode = "1234"
                },
                new Card { Id = Guid.NewGuid(),
                           Balance = 20000,
                           Number = "2222222222222222",
                           IsBlocked = false,
                           PinCode = "4321"
                }
                );
        }
    }
}
