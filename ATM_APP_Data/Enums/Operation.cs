﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATM_APP_Data.Enums
{
    public enum Operation
    {
        Withdrawal,
        CheckBalance
    }
}
