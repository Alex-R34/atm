﻿using ATM_App.Models.ATM;
using ATM_APP_Data.Context;
using Microsoft.AspNetCore.Mvc;
using ATM_APP_Data.Entities;
using Microsoft.EntityFrameworkCore;
using ATM_App.Models.Errors;
using ATM_APP_Data.Enums;

namespace ATM_App.Controllers
{
    public class OperationsController : Controller
    {
        private readonly AtmContext _context;

        public OperationsController(AtmContext context)
        {
            _context = context;
        }

        public IActionResult Index(CardNumberViewModel model)
        {
            return View(model);
        }

        [HttpGet]
        public IActionResult Withdrawal(CardNumberViewModel model)
        {
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> WithdrawFunds(CardWithdrawalViewModel cardInfo)
        {
            var card = await _context.Cards.FirstOrDefaultAsync(x => x.Number == cardInfo.Number);

            if (card == null)
                return View("Error", new CustomErrorViewModel { Message = "Карта не была найдена." });

            if (card.Balance < cardInfo.Withdrawal)
                return View("Error", new CustomErrorViewModel { Message = "Недостаточно средств." });

            card.Balance = card.Balance - cardInfo.Withdrawal;
            await _context.Operations.AddAsync(new Operations
            {
                CardId = card.Id,
                CardNumber = card.Number,
                DateTime = DateTime.Now,
                Operation = Operation.Withdrawal,
                WithdrawalAmount = cardInfo.Withdrawal
            });

            await _context.SaveChangesAsync();

            return View("FundsWithdrawn", new FundsWithdrawnViewModel { CardNumber = card.Number, Withdrawn = cardInfo.Withdrawal, FundsLeft = card.Balance });
        }

        [HttpGet]
        public async Task<IActionResult> CardBalance(CardNumberViewModel cardInfo)
        {
            var card = _context.Cards.FirstOrDefault(x => x.Number == cardInfo.CardNumber);

            if (card == null)
                return View("Error", new CustomErrorViewModel { Message = "Карта не была найдена." });

            await _context.Operations.AddAsync(new Operations
            {
                CardId = card.Id,
                CardNumber = cardInfo.CardNumber,
                DateTime = DateTime.Now,
                Operation = ATM_APP_Data.Enums.Operation.CheckBalance
            });
            await _context.SaveChangesAsync();

            return View(new CardBalanceViewModel { Number = cardInfo.CardNumber, Balance = card.Balance });
        }
    }
}
