﻿using ATM_App.Models;
using ATM_App.Models.ATM;
using ATM_App.Models.Errors;
using ATM_APP_Data.Context;
using ATM_APP_Data.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ATM_App.Controllers
{
    public class AtmController : Controller
    {
        private readonly AtmContext _context;
        private const int MaxFailedAttemps = 4;

        public AtmController(AtmContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> CheckCardNumber(CardNumberViewModel card)
        {
            if (!ModelState.IsValid)
                return View("Error", new CustomErrorViewModel { Message = "Введённый данные не валидны." });

            var cardExists = await _context.Cards.AnyAsync(x => x.Number == card.CardNumber);

            if (!cardExists)
                return View("Error", new CustomErrorViewModel { Message = "Карты с таким номером не сущетвует."});

            return View(nameof(CheckPinCode), card);
        }

        [HttpPost]
        public async Task<IActionResult> CheckPinCode(PinCodeViewModel pinCode, CardNumberViewModel cardNum)
        {
            if (!ModelState.IsValid)
                return View("Error", new CustomErrorViewModel { Message = "Введённый данные не валидны." });

            var card = await _context.Cards.FirstOrDefaultAsync(x => x.Number == cardNum.CardNumber);

            if (card == null)
                return View("Error", new CustomErrorViewModel { Message = "Карты с таким номером не сущетвует." });

            if (card.IsBlocked)
                return View("Error", new CustomErrorViewModel { Message = "Карта заблокированна." });

            if (card.FailedAttemps >= MaxFailedAttemps)
            {
                card.IsBlocked = true;
                await _context.SaveChangesAsync();
                return View("Error", new CustomErrorViewModel { Message = "Превышен лимит ввода ПинКода, карта заблокированна." });
            }

            if (card.PinCode != pinCode.Code)
            {
                card.FailedAttemps++;
                await _context.SaveChangesAsync();
                return View("CheckPinCode", cardNum);
            }

            if (card.FailedAttemps > 0)
            {
                card.FailedAttemps = 0;
                await _context.SaveChangesAsync();
            }

            return RedirectToAction("Index", "Operations", cardNum);
        }

    }
}
