﻿namespace ATM_App.Models.Errors
{
    public class CustomErrorViewModel
    {
        public string Message { get; set; }
    }
}
