﻿using System.ComponentModel.DataAnnotations;

namespace ATM_App.Models.ATM
{
    public class PinCodeViewModel
    {
        [RegularExpression("[0-9]{4}")]
        public string Code { get; set; }
    }
}
