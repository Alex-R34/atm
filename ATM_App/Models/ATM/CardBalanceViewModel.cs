﻿namespace ATM_App.Models.ATM
{
    public class CardBalanceViewModel
    {
        public string Number { get; set; }
        public decimal Balance { get; set; }
    }
}
