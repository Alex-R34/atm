﻿using System.ComponentModel.DataAnnotations;

namespace ATM_App.Models.ATM
{
    public class CardNumberViewModel
    {
        [Required]
        [RegularExpression("[0-9]{16}")]
        public string CardNumber { get; set; }
    }
}
