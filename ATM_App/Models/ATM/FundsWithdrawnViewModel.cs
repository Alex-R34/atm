﻿namespace ATM_App.Models.ATM
{
    public class FundsWithdrawnViewModel
    {
        public string CardNumber { get; set; }
        public decimal Withdrawn { get; set; }
        public decimal FundsLeft { get; set; }
    }
}
