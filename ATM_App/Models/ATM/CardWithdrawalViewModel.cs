﻿using System.ComponentModel.DataAnnotations;

namespace ATM_App.Models.ATM
{
    public class CardWithdrawalViewModel
    {
        [Required]
        [RegularExpression("[0-9]{16}")]
        public string Number { get; set; }
        [Required]
        public decimal Withdrawal { get; set; }
    }
}
