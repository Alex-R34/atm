using ATM_APP_Data.Context;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

#region Services


builder.Services.AddControllersWithViews();
builder.Services.AddDbContext<AtmContext>(options => options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")));


#endregion

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Atm}/{action=Index}/{id?}");

app.Run();
